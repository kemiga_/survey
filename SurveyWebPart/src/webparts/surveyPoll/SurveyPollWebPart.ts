import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneDropdown,
  IPropertyPaneDropdownOption
} from '@microsoft/sp-property-pane';
import { SPHttpClient  , SPHttpClientResponse , HttpClient,IHttpClientOptions
  , HttpClientResponse } from '@microsoft/sp-http';

import { escape } from '@microsoft/sp-lodash-subset';


import * as strings from 'SurveyPollWebPartStrings';
import "jquery";
import "popper.js";
import "bootstrap";
require("../../../node_modules/bootstrap/dist/css/bootstrap.min.css");
require ('../../../node_modules/@fortawesome/fontawesome-free/css/all.css') ;
require("../../assets/poll.css");

// Chart //
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_material  from "@amcharts/amcharts4/themes/material";

export interface ISurveyPollWebPartProps {
  description: string;
  contentSite2:string ;
  listTitlePoll1: string;
  listTitlePoll2: string;
  listTitlePoll3: string;
  slidetimepoll : number;
  labelpoll : string ; 
}

export interface ISPLists {
  value: ISPList[];
}

export interface ISPList {
  Title: string;
  Id: string;
  Url:string;
  
}

export default class SurveyPollWebPart extends BaseClientSideWebPart<ISurveyPollWebPartProps> {
  protected listFetch1 :boolean=false;
  protected lastSelected1 : string="";
  protected listFetch2 :boolean=false;
  protected lastSelected2 : string="";
  private listItemEntityTypeName: string = undefined;
  private surveyName:string = "" ;
  public render(): void {
    let slidetimepoll :any = this.properties.slidetimepoll*1000;
    
    if(this.properties.labelpoll == "" || this.properties.labelpoll == null )
      this.surveyName = "แบบประเมินความพึงพอใจ";
     else 
      this.surveyName = this.properties.labelpoll ;  
    this.domElement.innerHTML = `

    <!-- Survey button -->
        <button type="button" class="btn " id="poll" ><i class="fas fa-poll-h fa-lg " ></i> ${this.surveyName}</button>
      <!-- End Survey 


    <div class="container-fluid poll"  > -->
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3" id="calendarpoll" style="display:none;">  
             
              <h5 class="poll-title"><i class="fas fa-poll-h" style="color:#ffc107"></i>  ${this.surveyName}
              <button type="button" id="closeVote" class="btn top-right" ><i class="fas fa-times"></i></button></h5>
              <hr border="1" style="margin-top:0;margin-bottom:0;">
                <div id="carousel-poll" class="carousel slide" data-interval="${slidetimepoll}" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators padingBTzero bottomzero" id="carousel-indicators-poll">
                      
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" id="spPoll" role="listbox">
                        
                    </div>
                </div>
           
        </div>
      </div>  

   <!-- </div> -->
      `;
      this._renderPoll();

      $('#poll').click(function() {
				$('#calendarpoll').css('display','block');
				$('#poll').css('display','none');
		
			});
			
			$('#closeVote').click(function() {
				$('#calendarpoll').css('display','none');
				$('#poll').css('display','block');
      });
  }
  private _renderPollAllPage(FieldItem:ISPList[],items: ISPList[],Page:number, UserID :string):void
  {
    if(FieldItem != null && items!=null)
    {
      let html:string= "";
      let Choices:string[] = FieldItem[0]["Choices"];
      let Title:string = FieldItem[0]["Title"];
      let FieldIndex:string = FieldItem[0]["EntityPropertyName"];
      let Result:number[] = new Array(Choices.length);
      let pieAns : any = [] ;
      let pieValue : any = [] ;
      let checkVote :number = 0;
      let displayPie = ["none","inline"];
      //set Result
      for(let setR:number = 0;setR<Result.length;setR++)
      {
        Result[setR] = 0;
      }
      for(let countVoteOut:number = 0 ;countVoteOut<Choices.length;countVoteOut++ )
      {
          for(let countVoteIn:number = 0;countVoteIn<items.length;countVoteIn++)
          {
            if(items[countVoteIn][FieldIndex]==Choices[countVoteOut])
            {
              Result[countVoteOut]++;
            }
          }
      }
      for(let itemlenth = 0  ;itemlenth< items.length ; itemlenth++)
      {
        if(items[itemlenth]["AuthorId"] == UserID)
        {
          checkVote = 1;
          break;
        }
      }

      if(Page ==1)
      {
        
        html+=`
        <div class="carousel-item active">
          <div class="container-fluid">
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pollpage${Page}" >
                <p class="PollTitle">${Title}
                <button class="btn btn-sm" title="Show Chart" style="display:${displayPie[checkVote]};" id="pieShow${Page}"><i class="fas fa-chart-pie"></i></button>
                </p>`;
        if(checkVote ==0)
        {
          for(let countVoteChoice:number=0;countVoteChoice<Choices.length;countVoteChoice++)
          {
            html+=`
            <div class="radio Choice">
              <label><input type="radio" id="Radio${countVoteChoice}Page${Page}" data-choice="${Choices[countVoteChoice]}" name="poll${Page}">${Choices[countVoteChoice]}</label>
            </div>`;
          }
          html+=`<div> <button type="button" id="VoteBtn${Page}" class="btn btn-votebtn btn-sm">Vote!</button> </div>`;
        }
        if(checkVote == 1)
        {
   
          //$(`#pieShow${Page}`).css('display','inline'); 
          html += `<div id="chartdiv${Page}" style="display:none;"></div>` ; 
          
          this._renderPieChart(Result,Choices,Page);
          for(let countVoteChoice:number=0;countVoteChoice<Choices.length;countVoteChoice++)
          {
            let percentBar:number = Math.round(((Result[countVoteChoice]/items.length)*100));
            let ariaValue :number = Math.round(Result[countVoteChoice]);
         
        

            if(isNaN(percentBar))
            {
              percentBar =0;
              ariaValue = 0;
            }
            if(percentBar<=85)
            {
              html+=`
              <div id="bardiv${Page}">
                <p class="Result">${Choices[countVoteChoice]}</p>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="${ariaValue}"
                aria-valuemin="0" aria-valuemax="${items.length}" style="width:${percentBar}%">
                </div> 
                <b> &nbsp&nbsp${percentBar}%</b>
              </div>`;
              
            }
            else
            {
              html+=`
              <div id="bardiv${Page}">
                <p class="Result">${Choices[countVoteChoice]}</p>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="${ariaValue}"
                aria-valuemin="0" aria-valuemax="${items.length}" style="width:${percentBar}%">
                <b>${percentBar}%</b>
                </div> 
  
              </div>`;
              
            }
            
          }        
        }


        html+=`
              </div>
            </div>
          </div>
        </div>
        `;
        //return html;
        
        const listpoll: Element = this.domElement.querySelector('#spPoll');
        listpoll.innerHTML += html;

      }
      else if(Page ==2 || Page ==3)
      {
        html+=`
        <div class="carousel-item">
          <div class="container-fluid">
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pollpage${Page}" >
                <p class="PollTitle">${Title}
                <button class="btn btn-sm"  title="Show Chart" style="display:${displayPie[checkVote]};" id="pieShow${Page}"><i class="fas fa-chart-pie"></i></button>
                </p>`;
        if(checkVote ==0)
        {
          for(let countVoteChoice:number=0;countVoteChoice<Choices.length;countVoteChoice++)
          {
            html+=`
            <div class="radio Choice">
              <label><input type="radio" id="Radio${countVoteChoice}Page${Page}" data-choice="${Choices[countVoteChoice]}" name="poll${Page}">${Choices[countVoteChoice]}</label>
            </div>`;
          }
          html+=`<div> <button type="button"  id="VoteBtn${Page}" class="btn btn-votebtn btn-sm">Vote!</button> </div>`;
        }
        if(checkVote == 1)
        {
         
         // $(`#pieShow${Page}`).css('display','inline'); 
          html += `<div id="chartdiv${Page}" style="display:none;"></div>` ; 
          
          this._renderPieChart(Result,Choices,Page);
          for(let countVoteChoice:number=0;countVoteChoice<Choices.length;countVoteChoice++)
          {
            let percentBar:number = Math.round(((Result[countVoteChoice]/items.length)*100));
            let ariaValue :number = Math.round(Result[countVoteChoice]);
            if(isNaN(percentBar))
            {
              percentBar =0;
              ariaValue = 0;
            }
            if(percentBar<=85)
            {
              html+=`
              <div id="bardiv${Page}">
                <p class="Result">${Choices[countVoteChoice]}</p>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="${ariaValue}"
                aria-valuemin="0" aria-valuemax="${items.length}" style="width:${percentBar}%">
                </div> 
                <b> &nbsp&nbsp${percentBar}%</b>
              </div>`;
            }
            else
            {
              html+=`
              <div id="bardiv${Page}">
                <p class="Result">${Choices[countVoteChoice]}</p>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="${ariaValue}"
                aria-valuemin="0" aria-valuemax="${items.length}" style="width:${percentBar}%">
                <b>${percentBar}%</b>
                </div> 
  
              </div>`;
            }
  
           
          }
  
        }





        html+=`
              </div>
            </div>
          </div>
        </div>`;
        //return html;
        
      

        const listpoll: Element = this.domElement.querySelector('#spPoll');
        listpoll.innerHTML += html;

      }

    }/// end if///
    else
    {
      //return 0;
    }
    
  }
  private _renderPoll(): void {
      
     
    let htmlIndicators:string = ``;
    let pollcount :number = 0;
    
    let poll1:number =0;
    let poll2:number =0;
    let poll3:number =0;
      let listpollinner :number = -1;
      if(this.properties.listTitlePoll1!=undefined&&this.properties.listTitlePoll1!="No Select"){
          poll1 = 1;
          pollcount++;
      }
      if(this.properties.listTitlePoll2!=undefined&&this.properties.listTitlePoll2!="No Select"){
          poll2 = 1;
          pollcount++;
      }
    
      if(this.properties.listTitlePoll3!=undefined&&this.properties.listTitlePoll3!="No Select"){
          poll3 = 1;
          pollcount++;
      }

      if(pollcount == 3)
      {
        htmlIndicators +=`
        <li data-target="#carousel-poll" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-poll" data-slide-to="1"></li>
        <li data-target="#carousel-poll" data-slide-to="2"></li>`;
        let listpollIndicator: Element = this.domElement.querySelector('#carousel-indicators-poll');
        listpollIndicator.innerHTML = htmlIndicators;
        this._getListPollFieldData(this.properties.listTitlePoll1).then((response)=>{
          let FieldItem :ISPList[] = response.value;
          this._getListPollVoteData(this.properties.listTitlePoll1,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
            //this._renderPollAllPage(FieldItem,responses.value,1);
            this._getListPollFieldData(this.properties.listTitlePoll2).then((response2)=>{
              let FieldItem2 :ISPList[] = response2.value;
              this._getListPollVoteData(this.properties.listTitlePoll2,FieldItem2[0]["EntityPropertyName"]).then((responses2)=>{
                //this._renderPollAllPage(FieldItem,responses.value,2);
                this._getListPollFieldData(this.properties.listTitlePoll3).then((response3)=>{
                  let FieldItem3 :ISPList[] = response3.value;
                  this._getListPollVoteData(this.properties.listTitlePoll3,FieldItem3[0]["EntityPropertyName"]).then((responses3)=>{
                    //this._renderPollAllPage(FieldItem,responses.value,3);
                    this._getListPollUserData().then((responseID)=>{
                      //console.log(responseID.Id);
                      this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                      this._renderPollAllPage(FieldItem2,responses2.value,2,responseID.Id);
                      this._renderPollAllPage(FieldItem3,responses3.value,3,responseID.Id);
                      let TitleName = this.properties.listTitlePoll1+"|"+this.properties.listTitlePoll2+"|"+this.properties.listTitlePoll3;
                      let FieldName = FieldItem[0]["EntityPropertyName"]+"|"+FieldItem2[0]["EntityPropertyName"]+"|"+FieldItem3[0]["EntityPropertyName"];
                      this._setClickVoteBtn(3,TitleName,FieldName);
                    });
                   
                  });
        
                });
              });
    
            });
          });

        });

       

       
      }
      else if(pollcount == 2)
      {
        htmlIndicators +=`
        <li data-target="#carousel-poll" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-poll" data-slide-to="1"></li>`;
        let listpollIndicator: Element = this.domElement.querySelector('#carousel-indicators-poll');
        listpollIndicator.innerHTML = htmlIndicators;
        if(poll1==1&&poll2==1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll1).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll1,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              //this._renderPollAllPage(FieldItem,responses.value,1);
              this._getListPollFieldData(this.properties.listTitlePoll2).then((response2)=>{
                let FieldItem2 :ISPList[] = response2.value;
                this._getListPollVoteData(this.properties.listTitlePoll2,FieldItem2[0]["EntityPropertyName"]).then((responses2)=>{
                  //this._renderPollAllPage(FieldItem,responses.value,2);
                  this._getListPollUserData().then((responseID)=>{
                    this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                    this._renderPollAllPage(FieldItem2,responses2.value,2,responseID.Id);
                    let TitleName = this.properties.listTitlePoll1+"|"+this.properties.listTitlePoll2;
                    let FieldName = FieldItem[0]["EntityPropertyName"]+"|"+FieldItem2[0]["EntityPropertyName"];
                    
                    this._setClickVoteBtn(2,TitleName,FieldName);
                  });
                 
                });
    
              });
  
            });
  
          });

         
         
        }
        else if(poll1 == 1 && poll3 ==1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll1).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll1,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              //this._renderPollAllPage(FieldItem,responses.value,1);
              this._getListPollFieldData(this.properties.listTitlePoll3).then((response2)=>{
                let FieldItem2 :ISPList[] = response2.value;
                this._getListPollVoteData(this.properties.listTitlePoll3,FieldItem2[0]["EntityPropertyName"]).then((responses2)=>{
                  //this._renderPollAllPage(FieldItem,responses.value,2);
                  this._getListPollUserData().then((responseID)=>{
                    this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                    this._renderPollAllPage(FieldItem2,responses2.value,2,responseID.Id);
                    let TitleName = this.properties.listTitlePoll1+"|"+this.properties.listTitlePoll3;
                    let FieldName = FieldItem[0]["EntityPropertyName"]+"|"+FieldItem2[0]["EntityPropertyName"];
                    this._setClickVoteBtn(2,TitleName,FieldName);
                  });
                 
                });
    
              });
  
            });
  
          });
        }
        else if(poll2 == 1 && poll3 ==1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll2).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll2,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              //this._renderPollAllPage(FieldItem,responses.value,1);
              this._getListPollFieldData(this.properties.listTitlePoll3).then((response2)=>{
                let FieldItem2 :ISPList[] = response2.value;
                this._getListPollVoteData(this.properties.listTitlePoll3,FieldItem2[0]["EntityPropertyName"]).then((responses2)=>{
                  //this._renderPollAllPage(FieldItem,responses.value,2);
                  this._getListPollUserData().then((responseID)=>{
                    this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                    this._renderPollAllPage(FieldItem2,responses2.value,2,responseID.Id);
                    let TitleName = this.properties.listTitlePoll2+"|"+this.properties.listTitlePoll3;
                    let FieldName = FieldItem[0]["EntityPropertyName"]+"|"+FieldItem2[0]["EntityPropertyName"];
                    this._setClickVoteBtn(2,TitleName,FieldName);
                  });
                  
                });
    
              });
  
            });
  
          });
         
        }
      }
      else if(pollcount == 1)
      {
        htmlIndicators +=`
        <li data-target="#carousel-poll" data-slide-to="0" class="active"></li>`;
        let listpollIndicator: Element = this.domElement.querySelector('#carousel-indicators-poll');
        listpollIndicator.innerHTML = htmlIndicators;
        if(poll1 == 1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll1).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll1,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              this._getListPollUserData().then((responseID)=>{
                this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                let TitleName = this.properties.listTitlePoll1;
                let FieldName = FieldItem[0]["EntityPropertyName"];
                this._setClickVoteBtn(1,TitleName,FieldName);
              });
              
            });

          });
        }
        else if(poll2 == 1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll2).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll2,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              this._getListPollUserData().then((responseID)=>{
                this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                let TitleName = this.properties.listTitlePoll2;
                let FieldName = FieldItem[0]["EntityPropertyName"];
                this._setClickVoteBtn(1,TitleName,FieldName);
              });
              
            });

          });
        }
        else if(poll3 == 1)
        {
          this._getListPollFieldData(this.properties.listTitlePoll3).then((response)=>{
            let FieldItem :ISPList[] = response.value;
            this._getListPollVoteData(this.properties.listTitlePoll3,FieldItem[0]["EntityPropertyName"]).then((responses)=>{
              this._getListPollUserData().then((responseID)=>{
                this._renderPollAllPage(FieldItem,responses.value,1,responseID.Id);
                let TitleName = this.properties.listTitlePoll3;
                let FieldName = FieldItem[0]["EntityPropertyName"];
                this._setClickVoteBtn(1,TitleName,FieldName);
              });
            
            });

          });
        }
      }
      
    }
  
    
  private getListItemEntityTypeName(TitleName:string): Promise<string> {
    return new Promise<string>((resolve: (listItemEntityTypeName: string) => void, reject: (error: any) => void): void => {
      /*if (this.listItemEntityTypeName) {
        resolve(this.listItemEntityTypeName);
        return;
      }*/
      if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
      {
        this.context.spHttpClient.get(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('${TitleName}')?$select=ListItemEntityTypeFullName`,
          SPHttpClient.configurations.v1,
          {
            headers: {
              'Accept': 'application/json;odata=nometadata',
              'odata-version': ''
            }
          })
          .then((response: SPHttpClientResponse): Promise<{ ListItemEntityTypeFullName: string }> => {
            return response.json();
          }, (error: any): void => {
            reject(error);
          })
          .then((response: { ListItemEntityTypeFullName: string }): void => {
            this.listItemEntityTypeName = response.ListItemEntityTypeFullName;
            resolve(this.listItemEntityTypeName);
        });
      }
      else
      {
        this.context.spHttpClient.get(`${this.properties.contentSite2}/_api/web/lists/getbytitle('${TitleName}')?$select=ListItemEntityTypeFullName`,
        SPHttpClient.configurations.v1,
        {
          headers: {
            'Accept': 'application/json;odata=nometadata',
            'odata-version': ''
          }
        })
        .then((response: SPHttpClientResponse): Promise<{ ListItemEntityTypeFullName: string }> => {
          return response.json();
        }, (error: any): void => {
          reject(error);
        })
        .then((response: { ListItemEntityTypeFullName: string }): void => {
          this.listItemEntityTypeName = response.ListItemEntityTypeFullName;
          resolve(this.listItemEntityTypeName);
      });
      }
    });
  }
  



  
  private createItem(TitleName:string,FieldName:string,ChoiceName:string): void {
    //this.updateStatus('Creating item...');
    
    this.getListItemEntityTypeName(TitleName)
      .then((listItemEntityTypeName: string): Promise<SPHttpClientResponse> => {
        let set: string = `{"__metadata":{"type":"${listItemEntityTypeName}"},"${FieldName}":"${ChoiceName}"}`;
        const body:string = set;
        if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
        {
          return this.context.spHttpClient.post(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('${TitleName}')/items`,
            SPHttpClient.configurations.v1,
            {
              headers: {
                'Accept': 'application/json;odata=nometadata',
                'Content-type': 'application/json;odata=verbose',
                'odata-version': ''
              },
              body: body
            });
        }
        else
        {
          return this.context.spHttpClient.post(`${this.properties.contentSite2}/_api/web/lists/getbytitle('${TitleName}')/items`,
          SPHttpClient.configurations.v1,
          {
            headers: {
              'Accept': 'application/json;odata=nometadata',
              'Content-type': 'application/json;odata=verbose',
              'odata-version': ''
            },
            body: body
          });
        }

      })
      .then((response: SPHttpClientResponse): Promise<ISPList> => {
        if(response.status == 201)
        {
          alert(`Vote successfully `);
          const listpoll: Element = this.domElement.querySelector('#spPoll');
          jQuery('#spPoll').fadeOut(400,()=>{
            listpoll.innerHTML ="";
          });
          this._renderPoll();
          
        }
        else if(response.status == 500)
        {
          alert(`You are not allowed to respond again to this survey.`);
        }
        else if(response.status == 400)
        {
          alert('Error while Voting');
        }
        
        return response.json();

      })
      .then((item: ISPList): void => {
        /*
        var str:string = item["odata.error"]["message"].value;
        if(str == "You are not allowed to respond again to this survey.")
        {
          alert(`You are not allowed to respond again to this survey.`);
        }
        else
        {
          alert(`Item '${item.Title}' (ID: ${item.Id}) successfully created`);
        }
      }, (error: any): void => {
        alert('Error while creating the item: ' + error);
        */
      });
  }
  
  private _SendVote(Pages:number,TitleName:string,FieldName:string):void
  {
    //alert(jQuery(`.pollpage${Pages} > .Choice`).length +" Page "+Pages);
    let ChoiceLength :number = jQuery(`.pollpage${Pages} > .Choice`).length;
    let SelectChoice : string ="";
    for(let chCount:number =0 ;chCount<ChoiceLength;chCount++)
    {
      if(jQuery(`#Radio${chCount}Page${Pages}`).prop("checked"))
      {
        SelectChoice = jQuery(`#Radio${chCount}Page${Pages}`).attr("data-choice");
        //alert(SelectChoice+"|"+TitleName+"|"+FiledName);
        this.createItem(TitleName,FieldName,SelectChoice);
        break;
      }
    }

  }

  private _setClickShowPie(Pages:number):void{
    
    for(let i:number = 1 ;i<=Pages;i++)
    {
      
      let btnVote:Element = this.domElement.querySelector(`#pieShow${i}`);
      // $(`#pieShow${i}`).click(function(){
        try
        {
          btnVote.addEventListener("click",()=>{
            if($(`#pieShow${i}`).hasClass('btn-primary')){
                $(`#pieShow${i}`).removeClass('btn-primary');
                $(`#chartdiv${i}`).css('display','none') ;
                $(`#bardiv${i}`).css('display','block') ;

            }
            else{
              $(`#chartdiv${i}`).css('display','block') ;
              $(`#pieShow${i}`).addClass('btn-primary') ;
              $(`#bardiv${i}`).css('display','none') ;
            }
         });
        }
        catch(ex) 
        {
          //console.log(ex.message);
        }
    }

  }
  private _setClickVoteBtn(Pages:number,TitleName:string,FieldName:string):void
  {

    //const listpoll: Element = this.domElement.querySelector('#spPoll');

    jQuery('#spPoll').fadeIn();
  

    for(let i:number = 1 ;i<=Pages;i++)
    {
      let btnVote:Element = this.domElement.querySelector(`#VoteBtn${i}`);
      $(`#pieShow${i}`).click(function(){
        if($(`#pieShow${i}`).hasClass('btn-primary')){
            $(`#pieShow${i}`).removeClass('btn-primary');
            $(`#chartdiv${i}`).css('display','none') ;
            $(`#bardiv${i}`).css('display','block') ;

        }
        else{
          $(`#chartdiv${i}`).css('display','block') ;
          $(`#pieShow${i}`).addClass('btn-primary') ;
          $(`#bardiv${i}`).css('display','none') ;
        }

      });
      try
      {
        btnVote.addEventListener("click",()=>{
          //this.RenderPreviousMonthCalendar();
          if(Pages>1)
          {
            this._SendVote(i,TitleName.split("|")[i-1],FieldName.split("|")[i-1]);
          }
          else
          {
            this._SendVote(i,TitleName,FieldName);
          }
          
          //alert(`#VoteBtn${i} Click`);
        });
      }
      catch(ex) 
      {
        //console.log(ex.message);
      }
      
    }
  }
  private _getListPollVoteData(listName: string,fieldName:string): Promise<ISPLists> {
    
    if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
    {
        const queryString: string = `$select=*,${fieldName}`;
    
        return this.context.spHttpClient
          .get(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists/GetByTitle('${listName}')/items?${queryString}`,
          SPHttpClient.configurations.v1)
          .then((response: SPHttpClientResponse) => {
            if (response.status === 404) {
              console.error('Poll', new Error('List not found.'));
              return [];
            }else {
              return response.json();
            }
          });
    }
    else
    {
      const queryString: string = `$select=*,${fieldName}`;
      
          return this.context.spHttpClient
            .get(`${this.properties.contentSite2}/_api/web/lists/GetByTitle('${listName}')/items?${queryString}`,
            SPHttpClient.configurations.v1)
            .then((response: SPHttpClientResponse) => {
              if (response.status === 404) {
                console.error('Poll', new Error('List not found.'));
                return [];
              }else {
                return response.json();
              }
            });
    }
  }

  
  private _getListPollFieldData(listName: string): Promise<ISPLists> {
    if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
    {
        const queryString: string = "$filter=(canbedeleted eq true)&items?$top=1&$select=EntityPropertyName,Title,Choices";
    
        return this.context.spHttpClient
          .get(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists/GetByTitle('${listName}')/fields?${queryString}`,
          SPHttpClient.configurations.v1)
          .then((response: SPHttpClientResponse) => {
            if (response.status === 404) {
              console.error('Poll', new Error('List not found.'));
              return [];
            }else {
              return response.json();
            }
          });
    }
    else
    {
        const queryString: string = "$filter=(canbedeleted eq true)&items?$top=1&$select=EntityPropertyName,Title,Choices";
      
          return this.context.spHttpClient
            .get(`${this.properties.contentSite2}/_api/web/lists/GetByTitle('${listName}')/fields?${queryString}`,
            SPHttpClient.configurations.v1)
            .then((response: SPHttpClientResponse) => {
              if (response.status === 404) {
                console.error('Poll', new Error('List not found.'));
                return [];
              }else {
                return response.json();
              }
            });
    }
  }


  private _getListPollUserData(): Promise<ISPList> {
    if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
    {
        //const queryString: string = "$filter=(canbedeleted eq true)&items?$top=1&$select=EntityPropertyName,Title,Choices";
    
        return this.context.spHttpClient
          .get(`${this.context.pageContext.web.absoluteUrl}/_api/web/currentuser`,
          SPHttpClient.configurations.v1)
          .then((response: SPHttpClientResponse) => {
            if (response.status === 404) {
              console.error('Poll', new Error('List not found.'));
              return [];
            }else {
              return response.json();
            }
          });
    }
    else
    {
        //const queryString: string = "$filter=(canbedeleted eq true)&items?$top=1&$select=EntityPropertyName,Title,Choices";
      
          return this.context.spHttpClient
            .get(`${this.properties.contentSite2}/_api/web/currentuser`,
            SPHttpClient.configurations.v1)
            .then((response: SPHttpClientResponse) => {
              if (response.status === 404) {
                console.error('Poll', new Error('List not found.'));
                return [];
              }else {
                return response.json();
              }
            });
    }
  }


   

private _getListTitlesPoll(): Promise<ISPLists> {
  if(this.properties.contentSite2 == undefined||this.properties.contentSite2 =="" ||this.properties.contentSite2 == "No Select")
  {
    return this.context.spHttpClient.get(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists?$filter=Hidden eq false`,
    SPHttpClient.configurations.v1)
    .then((response: SPHttpClientResponse) => {
      return response.json();
    });
  }
  else
  {
    return this.context.spHttpClient.get(`${this.properties.contentSite2}/_api/web/lists?$filter=Hidden eq false`,
    SPHttpClient.configurations.v1)
    .then((response: SPHttpClientResponse) => {
      if(response.status==200)
      {
        return response.json();
      }
      else
      {
        return this.context.spHttpClient.get(`${this.context.pageContext.web.absoluteUrl}/_api/web/lists?$filter=Hidden eq false`,
        SPHttpClient.configurations.v1)
        .then((response: SPHttpClientResponse) => {
          return response.json();
        });
      }
    });
  }
}


private _renderPieChart(Choice:any,Result:any,Page:number):void{
  $(document).click(function(){

    am4core.useTheme(am4themes_material);
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create("chartdiv"+Page, am4charts.PieChart);
    let data:string = "" ; 

   for(let i:number=0 ; i < Result.length ; i++ ){


   }
 //Add data
   chart.data = [{"Result":Result[0],"Choice":Choice[0]},
   {"Result":Result[1],"Choice":Choice[1]},
   {"Result":Result[2],"Choice":Choice[2]}
   ] ; 
  

      // Add and configure Series
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "Choice";
      pieSeries.dataFields.category = "Result";
      pieSeries.slices.template.stroke = am4core.color("#fff");
      pieSeries.slices.template.strokeWidth = 2;
      pieSeries.slices.template.strokeOpacity = 1;
      
      // This creates initial animation
      pieSeries.hiddenState.properties.opacity = 1;
      pieSeries.hiddenState.properties.endAngle = -90;
      pieSeries.hiddenState.properties.startAngle = -90;

      //Legend 
      // chart.legend = new am4charts.Legend();

      // Disable ticks and labels
      pieSeries.labels.template.disabled = true;
      pieSeries.ticks.template.disabled = true;
  });    




}


  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  private _dropdownOptions: IPropertyPaneDropdownOption[] = [];
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
     ///poll
  if(this.properties.contentSite2!= undefined )
  {
    this.listFetch2 = true;
    //this.lastSelected2 = this.properties.contentSite2;
  }
  if (this.listFetch2) {
    this._getListTitlesPoll()
    .then((response) => {

      let datalist :ISPList[] = response.value;
      let templist :ISPList = {Id:null,Title:"No Select",Url:""};
      datalist= response.value.concat(templist);
      this._dropdownOptions = datalist.map((list: ISPList) => {
        return {
          key: list.Title,
          text: list.Title
        };
      });
      this.listFetch2 = false;
      this.context.propertyPane.refresh();
    });
  }

    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneTextField('contentSite2', {
                  label: 'Poll Site Url:'
                }),
                PropertyPaneTextField('labelpoll', {
                  label: 'Survey Name :'
                }),
                PropertyPaneDropdown('listTitlePoll1', {
                  label: 'List Title Poll1',
                  options: this._dropdownOptions
                })
                ,
                PropertyPaneDropdown('listTitlePoll2', {
                  label: 'List Title Poll2',
                  options: this._dropdownOptions
                })
                ,
                PropertyPaneDropdown('listTitlePoll3', {
                  label: 'List Title Poll3',
                  options: this._dropdownOptions
                }),
                PropertyPaneTextField('slidetimepoll', {
                  label: 'Set Poll Slide Time (Second)'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
