declare interface ISurveyPollWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'SurveyPollWebPartStrings' {
  const strings: ISurveyPollWebPartStrings;
  export = strings;
}
